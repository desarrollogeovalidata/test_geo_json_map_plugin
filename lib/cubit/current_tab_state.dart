part of 'current_tab_cubit.dart';

class CurrentTabState extends Equatable {
  final int currentTab;

  const CurrentTabState({this.currentTab = 0});

  @override
  List<Object> get props => [currentTab];
}
