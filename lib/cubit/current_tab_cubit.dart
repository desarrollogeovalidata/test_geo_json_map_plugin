import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'current_tab_state.dart';

class CurrentTabCubit extends Cubit<CurrentTabState> {
  CurrentTabCubit() : super(CurrentTabState());

  void setCurrentTab(int value) => emit(CurrentTabState(currentTab: value));
}
