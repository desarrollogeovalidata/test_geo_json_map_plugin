import 'package:flutter/material.dart';
import 'package:test_geo_json_map_plugin/views/pages/home_page.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomePage());
      default:
        return null;
    }
  }
}
