import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_geo_json_map_plugin/cubit/current_tab_cubit.dart';
import 'package:test_geo_json_map_plugin/views/pages/mapa_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late GlobalKey stackKey;
  late List<Widget> screens;

  late final PageStorageBucket bucket;
  late Widget currentScreen;

  @override
  void initState() {
    super.initState();
    stackKey = GlobalKey();
    bucket = PageStorageBucket();

    screens = [
      MapaPage(),
      Placeholder(),
    ];

    currentScreen = screens[0];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          child: AppBar(
            brightness: Brightness.dark,
            backgroundColor: Theme.of(context).primaryColor,
            elevation: 0,
          ),
          preferredSize: Size.fromHeight(0),
        ),
        body: Stack(
          key: stackKey,
          children: [
            Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 44,
                  color: Theme.of(context).primaryColor,
                  alignment: Alignment.center,
                  child: Text(
                    'APP TEST',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                    child: PageStorage(
                  child: currentScreen,
                  bucket: bucket,
                )),
              ],
            ),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 10,
          child: Container(
            height: 60.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    MaterialButton(
                      minWidth: 40.0,
                      onPressed: () {
                        int currentTab = 0;
                        BlocProvider.of<CurrentTabCubit>(context)
                            .setCurrentTab(currentTab);
                        currentScreen = screens[currentTab];
                        setState(() {});
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.app_registration,
                            color: BlocProvider.of<CurrentTabCubit>(context)
                                        .state
                                        .currentTab ==
                                    0
                                ? Colors.teal
                                : Colors.grey,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 2.0),
                            child: Text('TAB 1',
                                style: TextStyle(
                                    color: BlocProvider.of<CurrentTabCubit>(
                                                    context)
                                                .state
                                                .currentTab ==
                                            0
                                        ? Colors.teal
                                        : Colors.grey,
                                    fontSize: 12.0)),
                          )
                        ],
                      ),
                    ),
                    MaterialButton(
                      minWidth: 40.0,
                      onPressed: () {
                        int currentTab = 1;
                        BlocProvider.of<CurrentTabCubit>(context)
                            .setCurrentTab(currentTab);
                        currentScreen = screens[currentTab];
                        setState(() {});
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.edit_location_alt,
                            color: BlocProvider.of<CurrentTabCubit>(context)
                                        .state
                                        .currentTab ==
                                    1
                                ? Colors.teal
                                : Colors.grey,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 2.0),
                            child: Text('TAB 2',
                                style: TextStyle(
                                    color: BlocProvider.of<CurrentTabCubit>(
                                                    context)
                                                .state
                                                .currentTab ==
                                            1
                                        ? Colors.teal
                                        : Colors.grey,
                                    fontSize: 12.0)),
                          )
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
