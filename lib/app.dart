import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'core/app_routes.dart';
import 'cubit/current_tab_cubit.dart';

class App extends StatefulWidget {
  final AppRouter appRouter;

  const App({
    Key? key,
    required this.appRouter,
  }) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final ThemeData _baseTheme = ThemeData(
    fontFamily: "Amaranth",
    accentColor: Colors.transparent,
    primaryColor: Colors.teal,
    canvasColor: Colors.white,
  );

  Future _init() async {
    if (Platform.isAndroid) {
      await Permission.storage.request();
      await Permission.location.request();
      await Permission.camera.request();
    }
  }

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.teal,
      statusBarIconBrightness: Brightness.light,
      // statusBarBrightness: Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));

    return MultiBlocProvider(
      providers: [
        BlocProvider<CurrentTabCubit>(
          create: (_) => CurrentTabCubit(),
          lazy: false,
        ),
      ],
      child: MaterialApp(
        title: "GB Logger",
        debugShowCheckedModeBanner: false,
        theme: _baseTheme,
        onGenerateRoute: this.widget.appRouter.onGenerateRoute,
        initialRoute: '/',
      ),
    );
  }
}
